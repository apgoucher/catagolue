This repository contains the Java source code for the backend of
[Catagolue](https://catagolue.hatsya.com), the largest distributed
search of random configurations in cellular automata.

Quick start
-----------

To run and initialise a local copy of Catagolue, run:

    bash run-catagolue-locally.sh

Then `http://localhost:8080/home` will behave like a normal copy of
`https://catagolue.hatsya.com/home`, but with data stored on your
disk instead of the cloud.

Licence
-------

The Catagolue source code itself (in `src/main/java`) is open-source
and licenced under the MIT licence. Third-party components (namely
the JavaScript files in `src/main/webapp/js`) are separate from the
Catagolue source code and are subject to their own licences.
