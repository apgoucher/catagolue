#!/bin/bash

bash configure-viewer.sh

cd apgmera
./recompile.sh --mingw
cd -

mkdir "src/main/webapp/autogen"
cp "apgmera/lifelib/genera/genuslist.py" "src/main/webapp/autogen/genuslist.py"

FILENAME="$( echo apgmera/apg* )"

if [ -f "$FILENAME" ]; then
cp "$FILENAME" "src/main/webapp/binaries/apgluxe-windows-x86_64.exe"
APGLUXE_VERSION="$( grep -o 'v[4-9][^-]*' 'apgmera/includes/base.h' )"
sed -i "s/v4.xxxx/$APGLUXE_VERSION/g" "src/main/java/com/cp4space/catagolue/servlets/ApgsearchServlet.java"
printf "\033[32;m FILENAME=$FILENAME APGLUXE_VERSION=$APGLUXE_VERSION \033[0m\n"
else
printf "\033[31;m Error: could not find precompiled apgsearch executable \033[0m\n"
fi

exit 0
