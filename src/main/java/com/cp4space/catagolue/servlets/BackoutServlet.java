package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.census.Census;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

public class BackoutServlet extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();

        if (!CensusServlet.isAdmin()) {
            writer.println("Haul backouts require superuser privileges.");
            return;
        }
        
        String haulcode = req.getParameter("haulcode");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String quantity = req.getParameter("quantity");
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (haulcode == null)) {
                haulcode = pathParts[3];
            }
            if ((pathParts.length >= 5) && (quantity == null)) {
                quantity = pathParts[4];
            }
        }
        
        if ((rulestring != null) && (symmetry != null) && (haulcode != null) && (quantity != null)) {
            long aq = Long.valueOf(quantity);
            
            Census census = new Census();
            census.backoutHaul(writer, rulestring, symmetry, haulcode, aq);
        }
    }
}
