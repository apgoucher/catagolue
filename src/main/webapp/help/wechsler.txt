Objects are stored in the following format (as a regular expression):

x[spq][0-9]+_[0-9a-z]+

The 'prefix' refers to everything before the underscore. I'll illustrate a few examples:

- xs40 means 'a still-life with 40 cells'
- xp15 means 'an oscillator of period 15'
- xq4 means 'a spaceship of period 4'

The string after the underscore is a representation of the object in Extended Wechsler Format:

Decoding
========

We extend a pattern notation developed by Allan Wechsler in 1992. For instance, xq4_27deee6 corresponds to a HWSS:

27deee6
.**....
**.****
.******
..****.
.......

More generally, the characters {0, 1, 2, ..., 8, 9, a, b, ..., w} correspond to the bitstrings {'00000', '00001', '00010', ..., '11111'}.

The character 'z' signifies a carriage return. For instance, consider xs31_0ca178b96z69d1d96:

0ca178b96
...**.**.
..*.*.*.*
.*..*...*
.**..***.
.........

69d1d96
.*****.
*.....*
*.*.*.*
.**.**.
.......

We use the characters 'w' and 'x' to abbreviate '00' and '000', respectively. So xp30_w33z8kqrqk8zzzx33 is the trans-queen-bee-shuttle:

0033
..**
..**
....
....
....

8kqrqk8
...*...
..***..
.*...*.
*.***.*
.*****.

[10 blank rows omitted]

00033
...**
...**
.....
.....
.....

Note that we don't include extraneous '0's at the ends of strips.

Finally, the symbols {'y0', 'y1', y2', ..., 'yx', 'yy', 'yz'}
correspond to runs of between 4 and 39 consecutive '0's. A good example
is the quadpole on ship, xp2_31a08zy0123cko:

31a08
**...
*.*..
.....
..*.*
.....

0000123cko
....*.*...
.....**...
.......**.
.......*.*
........**

This is everything you need to know to decode the Extended Wechsler
Format. In order to enforce a canonical form, we have further rules
regarding encoding:

Encoding
========

Make sure the leftmost column and uppermost row each contain at least
one live cell. (This gives a canonical position.)

Ignore any '0's on the ends of strips, and *always* replace {'00',
'000', '0000', '00000', ...} with {'w', 'x', 'y0', 'y1', 'y2', ...}.

(Theoretically, for runs of more than 39 zeroes, replace the first 39
zeroes with 'yz' and continue. At the moment my search script labels
everything larger than 40-by-40 as 'oversized' and refuses to process
it.)

So far, the encoding is guaranteed to be canonical for a given
orientation and phase. For example, with the caterer (p3 oscillator
with no symmetry), there are three phases and eight orientations, so
we have 24 possible encodings. We define a total order on these
encodings as follows:

- We prefer shorter representations to longer representations;
- For representations of the same length, apply lexicographical
ASCII ordering (and give preference to earlier strings).

This gives, for any still-life, oscillator or spaceship, an unambiguous
canonical code to represent the pattern. It has several desirable
properties:

Compression: it's much more compact than RLE or SOF for storing very
small patterns, and often even beats the common name ('xp15_4r4z4r4'
is shorter than 'pentadecathlon')!

Character set: it only uses digits, lowercase letters and the
underscore, so can be safely used in filenames and URLs.

Human-readability: the prefix means that we can instantly see whether
a particular object is a still-life (and if so, what size), oscillator
(and if so, what period) or spaceship (and if so, what period). It
also means that the string is instantly recognised as being an encoding
of an object (xp2_7 is obviously a blinker, whereas the digit 7 on its
own with no extra context is ambiguous).